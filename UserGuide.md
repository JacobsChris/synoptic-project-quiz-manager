# TODO: write user guide

# User Guide

## Information About This Guide

This guide assumes that the User has full permissions for the application, therefore it will cover some pages and some
content that not all Users can see. Whenever these are covered, the Guide will let you know who cannot see what. For all
pages, the User must be logged in to see any content. r pages may look slightly different, as the pages in this guide
have had most styling removed for ease of display.

## Home Page

![A screenshot of the Home Page.](screenshots/home_page.png)
All users can see the navigation sidebar on the left, and the dynamic content in the middle. The number of quizzes and
questions will update, as they are added; and the site will track visits to its Home Page.

The three hyperlinks will work for all users:

* clicking the log out link will redirect users to the log in page
* clicking the Home link will redirect users to this page.
* clicking the All Quizzes and All Questions buttons will direct people to the lists of QUizzes and Questions.

The sidebar is shown on all pages.

## All Quizzes

![The Quizzes List page, with no quizzes](screenshots/quizzes_list.png)

Currently, there are no quizzes. As a User with Edit access, we can see `Click Here to add a new quiz` which no other
user can see. All users will see the statement that no quizzes exist in the manager.

When there are quizzes in the list, they will displayed in a numbered list as seen below:
![The Quizzes List page, with two quizzes](screenshots/quizzes_list_with_quizzes.png)
Each quiz is also a direct link to its Details page. You can see who created each quiz as well as who last modified them

## Creating a Quiz

Following the link from the previous page will take us to the Quiz Management form.  
Here you can enter the name of a Quiz, and then submit it. You cannot add Questions to a Quiz from this page at this
time.
![Quiz Management Page, with a name entered](screenshots/quiz_management_page.png)

When you click submit you will be taken to the details page for that quiz.

## Quiz Details Page

![The Quiz Detail Page](screenshots/quiz_details_page_no_questions.png)

Currently, there are no questions associated with this Quiz. We will add them later. This page details who created the
Quiz, as well as the last person to modify it and when that occured.

Only those with Edit permissions will see the two links at the bottom. Choosing to modify the quiz will return you to
the Quiz Management page where you can rename it if you so wish. Choosing to delete it will navigate you to a
confirmation page, and you will eventually be redirected to the Quiz List after receiving confirmation.

![The Quiz Detail Page with questions](screenshots/quiz-details_with_question.png)

Once you have added questions, it will look something like this. Each question is a link to that questions details page.

## Questions List Page

![The Question List Page with no questions](screenshots/questions_list_no_questions.png)
This page is very similar to the Quiz List page, but displays questions instead. Again there are currently no questions,
and again only those with Edit permission can see link to add a new question.

Once you have added a question in the next step, it will look something like this:
![The Question List Page with no questions](screenshots/question_list_with_question.png)
Each question is again a link to the details page on that specific question.

## Question Management Page

Clicking the link to create a new question, or updating a question, will bring you to this page:
![The Question management page with some inputs filled in with palceholders](screenshots/question_management.png)
Here you can add your question, associate with as many quizzes as you wish, and provide answers for it. You can mark
answers correct, with no maximum or minimum amount required. If you need more answers, submitting the first three and
then editing the question will add three more answer fields. There is no limit to the number of Answers made, and any
empty fields will not be displayed.

Once you submit, you will be taken to the details view of that page.

## Question Details Page

![The Questions Detail page. It has several palceholder answers ans is associated with two quizzes](screenshots/question_details.png)
As with the Quiz Details page, you can see the original author, and the last person to modify the question, and the date
they did so at. You can also see all Quizzes associated with this question, in a numbered list.

If you have the View Answers permission, you can also see the answers. Those in the Restricted Group will not see the
answers to a question. Those in the Edit Group will once again see the links to modify or delete the question.

Choosing to modify this question will return you to the question management page. Choosing to delete it will bring you
to a confirmation page and then ultimately back to the Question List page, similarly to the Quiz pages. Choosing to
delete a question will also delete all of its answers.
