from django.contrib.auth.models import Group, User
from django.test import TestCase

import StartUp


class Test(TestCase):
    def setUp(self) -> None:
        StartUp.create_groups()
        StartUp.create_users()

    def test_groups_exist(self):
        list_of_groups = []
        for group_name in StartUp.GROUPS:
            # Assert the desired groups exist
            group = Group.objects.get(name=group_name)
            assert group
            list_of_groups += [group]

        assert len(list_of_groups) == 3

    def test_users_exist(self):
        for user_name in StartUp.USERS:
            user = User.objects.get(username=user_name)
            assert user

        assert len(User.objects.all()) == 4
