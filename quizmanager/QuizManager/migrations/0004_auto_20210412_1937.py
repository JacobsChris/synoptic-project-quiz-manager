# Generated by Django 3.2 on 2021-04-12 18:37

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("QuizManager", "0003_auto_20210412_1934"),
    ]

    operations = [
        migrations.RenameField(
            model_name="answer",
            old_name="isCorrect",
            new_name="is_correct",
        ),
        migrations.RenameField(
            model_name="answer",
            old_name="lastEdited",
            new_name="last_edited",
        ),
        migrations.RenameField(
            model_name="question",
            old_name="lastEdited",
            new_name="last_edited",
        ),
        migrations.RemoveField(
            model_name="quiz",
            name="lastEdited",
        ),
        migrations.AddField(
            model_name="quiz",
            name="last_edited",
            field=models.DateField(auto_now=True, verbose_name="Last Edited on:"),
        ),
    ]
