from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.forms.models import inlineformset_factory
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views import generic
from django.views.generic import CreateView, UpdateView, DeleteView

from .models import Question, Quiz, Answer


@login_required
def index(request) -> render:
    """
    The view function for the home page of the website.
    :param request: the request received
    :return: renders the page index.html
    """

    # How many quizzes and questions are there.
    num_questions = Question.objects.all().count()
    num_quizzes = Quiz.objects.all().count()

    # Number of people who visit the site
    visitors = request.session.get("visitor_count", 1)
    request.session["visitor_count"] = visitors + 1

    # The keys are used in the index.html file.  The template variables there must match the keys here.
    context = {
        "num_questions": num_questions,
        "num_quizzes": num_quizzes,
        "visitor_count": visitors,
    }

    return render(request, "index.html", context=context)


#######


# Quiz views below


#######


class QuizListView(LoginRequiredMixin, generic.ListView):
    model = Quiz
    paginate_by = 10


class QuizDetailView(LoginRequiredMixin, generic.DetailView):
    model = Quiz


class QuizCreate(PermissionRequiredMixin, CreateView):
    permission_required = "QuizManager.add_quiz"

    model = Quiz
    fields = ["name"]
    initial = {"name": "This is a placeholder name.  Replace me."}

    def form_valid(self, form):
        form.instance.author = self.request.user
        form.instance.updated_by = self.request.user
        return super().form_valid(form)


class QuizUpdate(PermissionRequiredMixin, UpdateView):
    permission_required = "QuizManager.change_quiz"

    model = Quiz
    fields = ["name"]

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super().form_valid(form)


class QuizDelete(PermissionRequiredMixin, DeleteView):
    permission_required = "QuizManager.delete_quiz"

    model = Quiz
    success_url = reverse_lazy("quizzes")


#######


# Question Views below


#######


class QuestionListView(LoginRequiredMixin, generic.ListView):
    model = Question
    paginate_by = 10


class QuestionDetailView(LoginRequiredMixin, generic.DetailView):
    model = Question


# Used to add and update answers alongside questions.
AnswerFormset = inlineformset_factory(Question, Answer, fields=("answer", "is_correct"))


class QuestionCreate(PermissionRequiredMixin, CreateView):
    permission_required = "QuizManager.add_question"

    model = Question
    fields = ["question", "quiz"]
    initial = {
        "question": "This is a placeholder question.  Replace me.",
    }

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.object = None

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        if self.request.POST:
            data["answers"] = AnswerFormset(
                self.request.POST,
            )
        else:
            data["answers"] = AnswerFormset()
        return data

    def form_valid(self, form):
        context = self.get_context_data()
        answers = context["answers"]
        form.instance.author = self.request.user
        form.instance.updated_by = self.request.user
        self.object = form.save()
        if answers.is_valid():
            answers.instance = self.object
            answers.save()
        return super().form_valid(form)


class QuestionUpdate(PermissionRequiredMixin, UpdateView):
    permission_required = "QuizManager.change_question"

    model = Question
    fields = ["question", "quiz"]

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.object = None

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        if self.request.POST:
            data["answers"] = AnswerFormset(
                self.request.POST,
                instance=self.object,
            )
        else:
            data["answers"] = AnswerFormset(
                instance=self.object,
            )
        return data

    def form_valid(self, form):
        context = self.get_context_data()
        answers = context["answers"]
        form.instance.updated_by = self.request.user
        self.object = form.save()
        if answers.is_valid():
            answers.instance = self.object
            answers.save()
        return super().form_valid(form)


class QuestionDelete(PermissionRequiredMixin, DeleteView):
    permission_required = "QuizManager.delete_question"

    model = Question
    success_url = reverse_lazy("questions")


#######


# Answer views below


#######


class AnswerCreate(PermissionRequiredMixin, CreateView):
    permission_required = "QuizManager.add_answer"

    model = Answer
    fields = ["answer", "question", "is_correct"]
    initial = {"answer": "This is a placeholder answer.  Replace me."}

    def form_valid(self, form):
        form.instance.author = self.request.user
        form.instance.updated_by = self.request.user
        return super().form_valid(form)


class AnswerUpdate(PermissionRequiredMixin, UpdateView):
    permission_required = "QuizManager.change_answer"

    model = Answer
    fields = ["answer", "question", "is_correct"]

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super().form_valid(form)


class AnswerDelete(PermissionRequiredMixin, DeleteView):
    permission_required = "QuizManager.delete_answer"

    model = Answer
    success_url = reverse_lazy("answers")
