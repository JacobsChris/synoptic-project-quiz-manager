from django.contrib import admin

from .models import Quiz, Question, Answer


# Register your models here.

# admin.site.register(Quiz)
# admin.site.register(Question)
# admin.site.register(Answer)


class AnswersInLine(admin.TabularInline):
    model = Answer


@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):
    list_display = ("question", "author", "updated_by", "last_edited")
    inlines = [AnswersInLine]


@admin.register(Answer)
class AnswerAdmin(admin.ModelAdmin):
    list_display = (
        "question",
        "answer",
        "is_correct",
        "last_edited",
    )


class QuestionsInLine(admin.TabularInline):
    model = Quiz.questions.through


@admin.register(Quiz)
class QuizAdmin(admin.ModelAdmin):
    list_display = ("name", "author", "updated_by", "last_edited")
    inline = [QuestionsInLine]
