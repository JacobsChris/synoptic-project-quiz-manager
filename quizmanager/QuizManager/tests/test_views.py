from django.contrib.auth.models import User, Permission
from django.test import TestCase
from django.urls import reverse

from QuizManager.models import Quiz, Question


class TestQuizListView(TestCase):
    test_user = None
    username = "TestUser"
    password = "testPassword"

    @classmethod
    def setUpTestData(cls):
        quiz_count = 15
        for quiz_id in range(quiz_count):
            Quiz.objects.create(name=f"Quiz {quiz_id}")

    def setUp(self) -> None:
        self.test_user = User.objects.create(username=self.username)
        self.test_user.set_password(raw_password=self.password)
        self.test_user.save()

    def test_url_redirects_if_not_logged_in(self):
        response = self.client.get(reverse("quizzes"))
        self.assertRedirects(response, "/accounts/login/?next=/QuizManager/quizzes/")

    def test_can_access_logged_in(self):
        login = self.client.login(username=self.username, password=self.password)
        response = self.client.get(reverse("quizzes"))

        assert login
        # Convert to string
        assert self.username == str(response.context["user"])
        assert response.status_code == 200
        self.assertTemplateUsed(response, "QuizManager/quiz_list.html")
        assert "is_paginated" in response.context
        assert response.context["is_paginated"]
        assert "quiz_list" in response.context
        assert len(response.context["quiz_list"]) == 10
        # because the page is paginated so five should be off the page


class TestQuizDetailView(TestCase):
    test_user = None
    username = "TestUser"
    password = "testPassword"

    @classmethod
    def setUpTestData(cls):
        Quiz.objects.create(name=f"Quiz")

    def setUp(self) -> None:
        self.test_user = User.objects.create(username=self.username)
        self.test_user.set_password(raw_password=self.password)
        self.test_user.save()

    def test_url_redirects_if_not_logged_in(self):
        response = self.client.get(reverse("quiz-detail", args=["1"]))
        self.assertRedirects(response, "/accounts/login/?next=/QuizManager/quiz/1")

    def test_can_access_logged_in(self):
        login = self.client.login(username=self.username, password=self.password)
        response = self.client.get(reverse("quiz-detail", args=["1"]))

        assert login
        # Convert to string
        assert self.username == str(response.context["user"])
        assert response.status_code == 200
        self.assertTemplateUsed(response, "QuizManager/quiz_detail.html")


class TestQuestionListView(TestCase):
    test_user = None
    username = "TestUser"
    password = "testPassword"

    @classmethod
    def setUpTestData(cls):
        question_count = 15
        for question_id in range(question_count):
            Question.objects.create(question=f"Question {question_id}")

    def setUp(self) -> None:
        self.test_user = User.objects.create(username=self.username)
        self.test_user.set_password(raw_password=self.password)
        self.test_user.save()

    def test_url_redirects_if_not_logged_in(self):
        response = self.client.get(reverse("questions"))
        self.assertRedirects(response, "/accounts/login/?next=/QuizManager/questions/")

    def test_can_access_logged_in(self):
        login = self.client.login(username=self.username, password=self.password)
        response = self.client.get(reverse("questions"))

        assert login
        # Convert to string
        assert self.username == str(response.context["user"])
        assert response.status_code == 200
        self.assertTemplateUsed(response, "QuizManager/question_list.html")
        assert "is_paginated" in response.context
        assert response.context["is_paginated"]
        assert "question_list" in response.context
        assert len(response.context["question_list"]) == 10
        # because the page is paginated so five should be off the page


class TestQuestionDetailView(TestCase):
    test_user = None
    username = "TestUser"
    password = "testPassword"

    @classmethod
    def setUpTestData(cls):
        Question.objects.create(question=f"Question")

    def setUp(self) -> None:
        self.test_user = User.objects.create(username=self.username)
        self.test_user.set_password(raw_password=self.password)
        self.test_user.save()

    def test_url_redirects_if_not_logged_in(self):
        response = self.client.get(reverse("question-detail", args=["1"]))
        self.assertRedirects(response, "/accounts/login/?next=/QuizManager/question/1")

    def test_can_access_logged_in(self):
        login = self.client.login(username=self.username, password=self.password)
        response = self.client.get(reverse("question-detail", args=["1"]))

        assert login
        # Convert to string
        assert self.username == str(response.context["user"])
        assert response.status_code == 200
        self.assertTemplateUsed(response, "QuizManager/question_detail.html")


class TestQuizAndQuestionManagementForms(TestCase):
    test_user = None
    username = "TestUser"
    password = "testPassword"

    @classmethod
    def setUpTestData(cls):
        Question.objects.create(question=f"Question")
        Quiz.objects.create(name=f"Quiz")

    def setUp(self) -> None:
        self.test_user = User.objects.create(username=self.username)
        self.test_user.set_password(raw_password=self.password)
        self.test_user.save()

    def test_forms_redirect_if_not_logged_in(self):
        """
        Cycle through all three forms for both models, and assert that the user is redirected if they are not logged in.
        """
        form_types = ["create", "update", "delete"]
        models = ["quiz", "question"]

        for model in models:
            for form_type in form_types:
                if form_type == "create":
                    response = self.client.get(reverse(f"{model}-{form_type}"))
                else:
                    response = self.client.get(
                        reverse(f"{model}-{form_type}", args=["1"])
                    )

                assert response.status_code == 302
                assert response.url.startswith("/accounts/login/")

    def test_forms_forbidden_if_not_have_perm(self):
        """
        Cycle through all three forms for both models, and assert that the user is forbidden as they do not have permission.
        """
        form_types = ["create", "update", "delete"]
        models = ["quiz", "question"]
        login = self.client.login(username=self.username, password=self.password)
        assert login

        for model in models:
            for form_type in form_types:
                if form_type == "create":
                    response = self.client.get(reverse(f"{model}-{form_type}"))
                else:
                    response = self.client.get(
                        reverse(f"{model}-{form_type}", args=["1"])
                    )
                print(f"Currently testing: can {form_type} {model}")
                assert response.status_code == 403

    def test_forms_allowed_if_have_perm(self):
        """
        Cycle through all three forms for both models, and assert that the user is forbidden as they do not have permission.
        """
        form_types = ["create", "update", "delete"]
        perm_types = ["view", "add", "delete", "change"]
        models = ["quiz", "question"]
        login = self.client.login(username=self.username, password=self.password)
        assert login

        for perm in perm_types:
            perm_name = f"Can {perm} question"
            add_perm = Permission.objects.get(name=perm_name)
            self.test_user.user_permissions.add(add_perm)
            self.test_user.save()
            perm_name = f"Can {perm} Quiz"
            add_perm = Permission.objects.get(name=perm_name)
            self.test_user.user_permissions.add(add_perm)
            self.test_user.save()

        for model in models:
            for form_type in form_types:
                if form_type == "create":
                    response = self.client.get(reverse(f"{model}-{form_type}"))
                else:
                    response = self.client.get(
                        reverse(f"{model}-{form_type}", args=["1"])
                    )
                print(f"Currently testing: can {form_type} {model}")
                print(self.test_user.get_all_permissions())
                assert response.status_code == 200
