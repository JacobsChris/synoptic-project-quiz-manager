import datetime

import pytz
from django.contrib.auth.models import User
from django.test import TestCase

from QuizManager.models import Quiz, Question, Answer


class TestQuiz(TestCase):
    @classmethod
    def setUpTestData(cls):
        test_user = User.objects.create(username="TestUser")
        Quiz.objects.create(name="Test Quiz", author=test_user, updated_by=test_user)

    def setUp(self) -> None:
        self.quiz = Quiz.objects.get(id=1)

    def test_question_update_field(self):
        date = self.quiz.last_edited
        date = date.strftime("%Y-%m-%d %H:%M")
        current_date = datetime.datetime.now(tz=pytz.utc).strftime("%Y-%m-%d %H:%M")
        assert date == current_date

    def test_quiz_fields(self):
        """
        These tests check that the custom fields on the quiz are correctly set.
        """

        name_label = self.quiz._meta.get_field("name").verbose_name
        assert name_label == "quiz-name"

        name_help_text = self.quiz._meta.get_field("name").help_text
        assert name_help_text == "Enter a quiz name"

        name_max_length = self.quiz._meta.get_field("name").max_length
        assert name_max_length == 200

    def test_quiz_get_absolute_url(self):
        assert self.quiz.get_absolute_url() == "/QuizManager/quiz/1"


class TestQuestion(TestCase):
    test_user = None

    @classmethod
    def setUpTestData(cls):
        test_user = User.objects.create(username="TestUser")
        Question.objects.create(
            question="Test question", author=test_user, updated_by=test_user
        )

    def setUp(self) -> None:
        self.test_user = User.objects.get(id=1)
        self.question = Question.objects.get(id=1)

    def test_question_update_field(self):
        date = self.question.last_edited
        date = date.strftime("%Y-%m-%d %H:%M")
        current_date = datetime.datetime.now(tz=pytz.utc).strftime("%Y-%m-%d %H:%M")
        assert date == current_date

    def test_question_fields(self):
        """
        These tests check that the custom fields on the questions are correctly set.
        """
        question_help_text = self.question._meta.get_field("question").help_text
        assert question_help_text == "Enter your question here."

        question_max_length = self.question._meta.get_field("question").max_length
        assert question_max_length == 200

        assert self.question.author == self.question.updated_by == self.test_user

    def test_question_get_absolute_url(self):
        assert self.question.get_absolute_url() == "/QuizManager/question/1"


class TestAnswer(TestCase):
    @classmethod
    def setUpTestData(cls):
        Answer.objects.create(answer="Test Answer", is_correct=True)

    def setUp(self) -> None:
        self.answer = Answer.objects.get(id=1)

    def test_answer_update_field(self):
        date = self.answer.last_edited
        date = date.strftime("%Y-%m-%d %H:%M")
        current_date = datetime.datetime.now(tz=pytz.utc).strftime("%Y-%m-%d %H:%M")
        assert date == current_date

    def test_answer_fields(self):
        """
        These tests check that the custom fields on the answers are correctly set.
        """
        answer_help_text = self.answer._meta.get_field("answer").help_text
        assert answer_help_text == "Enter your answer here."

        answer_max_length = self.answer._meta.get_field("answer").max_length
        assert answer_max_length == 200

        is_correct_help_text = self.answer._meta.get_field("is_correct").help_text
        assert is_correct_help_text == "Check if this answer is correct."
