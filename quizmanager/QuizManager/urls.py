from django.conf import settings
from django.conf.urls.static import static
from django.urls import path

from . import views

urlpatterns = [
    # Index Page
    path("", views.index, name="index"),
    # End Indexes
    # Start Quizzes
    # List of quizzes
    path("quizzes/", views.QuizListView.as_view(), name="quizzes"),
    # Specific Quiz
    path("quiz/<int:pk>", views.QuizDetailView.as_view(), name="quiz-detail"),
    # Quiz CUD
    # Quiz Create
    path("quizzes/create/", views.QuizCreate.as_view(), name="quiz-create"),
    # Quiz Update
    path(
        "quiz/<int:pk>/update/",
        views.QuizUpdate.as_view(),
        name="quiz-update",
    ),
    # QUiz Delete
    path(
        "quiz/<int:pk>/delete/",
        views.QuizDelete.as_view(),
        name="quiz-delete",
    ),
    # End Quizzes
    # Start Questions
    # List of Questions
    path("questions/", views.QuestionListView.as_view(), name="questions"),
    # Specific Question
    path(
        "question/<int:pk>", views.QuestionDetailView.as_view(), name="question-detail"
    ),
    # Questions CUD
    # Question Create
    path("questions/create/", views.QuestionCreate.as_view(), name="question-create"),
    # Question Update
    path(
        "question/<int:pk>/update/",
        views.QuestionUpdate.as_view(),
        name="question-update",
    ),
    # Question Delete
    path(
        "question/<int:pk>/delete/",
        views.QuestionDelete.as_view(),
        name="question-delete",
    ),
    # End Questions
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
