from django.conf import settings
from django.db import models

# Create your models here.
from django.urls import reverse


class Quiz(models.Model):
    """
    A model representing a list of QuizManager in a quiz.
    """

    id = models.AutoField(primary_key=True)
    name = models.CharField(
        max_length=200, help_text="Enter a quiz name", verbose_name="quiz-name"
    )
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.SET_NULL,
        null=True,
        related_name="original_quiz_author",
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.SET_NULL,
        null=True,
        related_name="update_quiz_author",
    )
    last_edited = models.DateTimeField(auto_now=True, verbose_name="Last Edited on:")
    questions = models.ManyToManyField(
        "Question", related_name="list_of_questions", blank=True
    )

    class Meta:
        verbose_name = "Quiz"
        verbose_name_plural = "Quizzes"

    def __str__(self):
        """
        String for representing the Model Object.
        :return: self.name
        """
        return self.name

    def get_absolute_url(self):
        return reverse("quiz-detail", args=[str(self.id)])


class Question(models.Model):
    """
    Model representing a question
    """

    id = models.AutoField(primary_key=True)
    question = models.CharField(max_length=200, help_text="Enter your question here.")
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.SET_NULL,
        null=True,
        related_name="original_question_author",
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.SET_NULL,
        null=True,
        related_name="update_question_author",
    )
    last_edited = models.DateTimeField(auto_now=True, verbose_name="Last Edited on:")
    quiz = models.ManyToManyField(Quiz, related_name="in_quizzes", blank=True)

    def __str__(self):
        """
        String for representing the Model Object.
        :return: self.name
        """
        return self.question

    def get_absolute_url(self):
        return reverse("question-detail", args=[str(self.id)])


class Answer(models.Model):
    """Model representing an answer"""

    answer = models.CharField(max_length=200, help_text="Enter your answer here.")
    question = models.ForeignKey(Question, on_delete=models.CASCADE, null=True)
    is_correct = models.BooleanField(help_text="Check if this answer is correct.")
    last_edited = models.DateTimeField(auto_now=True, verbose_name="Last Edited on:")

    def __str__(self):
        """
        String for representing the Model Object.
        :return: self.name
        """
        return self.answer
