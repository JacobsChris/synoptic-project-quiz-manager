import os

import django

import DjangoApp.settings

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "DjangoApp.settings")
django.setup()

from django.contrib.auth.models import Group, Permission, User

"""
This file checks whether the required groups exist, and makes them if they do not.
It then checks if the test users exists, and creates them if they do not.
"""

GROUPS = ["edit", "view", "restricted"]
MODELS = ["answer", "Quiz", "question"]
PERMS = ["view", "add", "delete", "change"]
USERS = ["superUser", "editUser", "viewUser", "restrictedUser"]


def create_groups(group_list=None, model_list=None, permission_list=None) -> None:
    """
    This function creates groups using the provided list of names and permissions.
    It is intended to be used only with the above globals.
    """
    if permission_list is None:
        permission_list = PERMS
    if model_list is None:
        model_list = MODELS
    if group_list is None:
        group_list = GROUPS

    for group_name in group_list:
        new_group, created = Group.objects.get_or_create(name=group_name)
        for model in model_list:
            if group_name == "edit":
                for perm in permission_list:
                    perm_name = f"Can {perm} {model}"
                    add_perm = Permission.objects.get(name=perm_name)
                    new_group.permissions.add(add_perm)
            elif group_name == "view":
                perm_name = f"Can view {model}"
                add_perm = Permission.objects.get(name=perm_name)
                new_group.permissions.add(add_perm)
            else:
                if model == "answer":
                    pass
                else:
                    perm_name = f"Can view {model}"
                    add_perm = Permission.objects.get(name=perm_name)
                    new_group.permissions.add(add_perm)


def create_users(list_of_usernames=None) -> None:
    """
    This function creates users using the provided list of usernames.
    It is intended to be used only with the above globals.
    """
    if list_of_usernames is None:
        list_of_usernames = USERS

    for username in list_of_usernames:
        try:
            User.objects.get(username=username)
        # Attempt to get the user, if the user does not exist, create them
        # If they do exist, nothing happens.
        except User.DoesNotExist:
            if username == "superUser":
                User.objects.create_superuser(
                    username=username,
                    email=f"{username}@email.com",
                    password=f"{username}password",
                )
            else:
                user = User.objects.create_user(
                    username=username,
                    email=f"{username}@email.com",
                    password=f"{username}password",
                )
                group_name, _ = username.split("User")
                group = Group.objects.get(name=group_name)
                user.groups.add(group)


if __name__ == "__main__":
    create_groups()
    if DjangoApp.settings.DEBUG:
        create_users()
