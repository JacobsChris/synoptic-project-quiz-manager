# Synoptic Project Quiz Manager

## Development Quick Start Instructions:

1. Clone the repository to your desired install location.
2. Install dependencies from `requirements.txt`, you should do this in a venv or pipenv.
   `Requirements.txt` can be found in the top level directory, where you found this `README.md`.
3. Run `QuizManager/HandleMigrations.sh`. It will check if there are migrations to make, and then applies. This will
   create a SQLite database for use.
4. Whilst in the `quizmanager/` directory, run `python manage.py runserver`. Depending on your setup, you may need to
   use `python3`.
5. To quickly create Users and Groups, including a superuser, run `QuizManager/StartUp.py`. Please note that you
   should **not** use the `CreateUsers()` in production, as the passwords are not secure.
6. To confirm that everything has been set up correctly, navigate to `127.0.0.1:8000/admin` and log in. You will be able
   to see the new created users and groups at this admin page. Navigate to  `http://127.0.0.1:8000/QuizManager/` to
   ensure that the User UI is also running. If you have any problems, please refer to the more detailed instructions
   further on in this guide.

## FAQ

### What's this for?

This is web based application to manage quizzes. It allows you to write and store questions, and their answers, and then
associate these with quizzes. One question can have many answers, the default assumption is three; but the application
also supports questions without answers. The application has no coded limit to the number of questions, but large
numbers may not be practical to display.

### Who's this for?

This application is designed for anyone who needs to store questions, and associated quizzes, and share them with
others. Example users may be teachers or other educators.

### How do I use it?

There are three categories of users:

* Restricted Users, who can view the Quizzes and Questions within, but cannot view the Answers.
* Viewing Users, who can view the Quizzes, Questions, and their Answers.
* Edit Users, who can view the Quizzes, Questions, and their Answers. They can also see and follow links to modify,
  create, or delete Quizzes and Questions. Answers are managed within their associated Questions.

The Website is only available to those with an account and can thus log in. If you need to create more Users, please
contact the Site Owner; if you are the Site Owner, see the detailed instructions further on.

### How do I run quizzes on this?

You cannot. This application does not run quizzes. It only stores and lists them.

### How many users does it support?

As the application currently used a SQLite database, large numbers of Users is not well-supported. If you find your
website requires more users, please refer to
the [Django documentation](https://docs.djangoproject.com/en/3.2/ref/databases/) on how to migrate from SQLite to an
alternative database. We recommend PostgreSQL.

## More Detailed Information and Instructions

QuizManager runs on Django, which is a Python Framework. Django manages many aspects of the application including:

* the connections to the provided database. Our default database is SQLite.
* the website pages, and their connections to one another
* Users, User Permissions, and User Authentication

For more information about Django, consider visiting [their website](https://www.djangoproject.com/)
and [their documentation](https://docs.djangoproject.com/en/3.2/ref/).

QuizManager handles Quizzes, Questions, and Answers - collectively referred hereafter to as the models. One quiz can
contain many questions, and one question can belong to many quizzes. One question can have many answers, but an answer
can only belong to one question. Database Tables are created for each of these models, and Django automatically creates
additional tables for many-to-many links, Users and User Groups.  
Below is an Entity Relationship Diagram, with User Groups not shown as they do not directly relate to the models. Tables
with a white-filled title are created and manged by Django.
![An entity relationship diagram showing the three models designed, as well as tables created and managed by Django.](screenshots/Quiz%20Manager%20Database%20ERD.png)

SQLite has been used for the Database as it provides a low weight and easily managed database. However, it does not
offer fast Creation, Update, or Deletion services. it is assumed that minimal users will have Edit permissions. That
means there will be minimal need to write new data to the database, and the chance of two or more users trying to change
the same data at once is negligible. If this turns out to be incorrect, we recommend migrating to PostgreSQL - for
information on this please refer to
the [official Django documentation](https://docs.djangoproject.com/en/3.2/ref/databases/).

### Details Instructions

#### Assumptions:

* You are comfortable using a terminal / command line
* You have installed Git to your computer, and know how to use it, at version 2.27.0 or newer
* You can run `.sh` files
* You have Python installed to at least version 3.8.6
* You have pip 21.0.1 installed

If any of these are not true, please refer to their own documentation to install or update as needed.

If you are not running on a linux-like environment, such as Ubuntu, please refer to your OS Documentation on how to
run `.sh` files, or manually run the `python manage.py makemigrations` and `python manage.py migrate` commands.

#### Step 1, cloning the project

Fork and then clone, or otherwise download, the repository. We recommend via SSH rather than HTTPS. If you download,
extract the compressed archive to your install location.

Confirm you have successfully cloned the project by navigating to the file structure from the command line. I managed
the project using PyCharm and so my working directory looks something
like `~/PycharmProjects/synoptic-project-quiz-manager/`, yours maye look different. Within this directory, you should be
able to see, among others, `requirements.txt` and `quizmanager/`. Confirm that within `quizmanager/`, the following
directories exist: `quizmanager/QuizManager/` and `quizmanager/DjangoApp`.

Unless you have reason to be on a different branch, confirm that you are on either the development or main branch by
running `git branch`. This will list all branches available, with the branch you are currently on indicated.

#### Step 2, installing requirements

First ensure that you are at the project root director, `synoptic-project-quiz-manager/` and can see the
file `requirements.txt`.

You will now create either a `venv` or `pipenv`. We recommend using a `venv` and have provided instructions for doing
so, however if you already know how to and prefer the use of `pipenv`, that is also supported.

Your IDE, such as PyCharm, may help in the creation of a `venv`, if so please follow their documentation. Otherwise, run
the command `python -m venv /venv/`. Depending on your Python installation, you may need to use `python3`. Confirm that
this exists by running `ls`. You should see, amongst other files and directories, `quizmanager/`,`venv/`,`pytest.ini`.

If you do not use a `venv`, you may have encounter permission errors as `pip install` will attempt to install these
modules globally. This is **not** recommended as it may cause dependency conflicts.

Having created a `venv`, you must now install all requirements found in `requirements.txt`. This is done by running the
following command: `pip install -r requirements.txt`. Ensure you run this command in the project root directory,
where `requirements.txt` is located. The output of this command should state which modules have been installed or were
already satisfied. If you need to confirm installation was successful, you can compare the output of `pip freeze` to the
contents of `requirements.txt`.

#### Step 3, create and manage database

Now that you have all your dependencies and requirements up to date, it is time to set up your database. We recommend
starting with SQLite, even if you intend to migrate to another database later on. These instructions assume you will
make a SQLite database.

In a terminal, locate and run `quizmanager/HandleMigrations.sh`. That's it. That will set up your database with the
required tables.

If, for whatever reason, you cannot run `HandleMigrations.sh`, you can run the following python commands manually.
Ensure you are in the same directory as the `quizmanager/manage.py` file. Then run `python manage.py makemigrations`
followed by `python manage.py migrate`. Depending on your python set up, you may need to use `python3`.

You should now be able to see `quizmanager/sqlite3` or similar. This is your database.

#### Step 4, run server

In a terminal, locate `quizmanager/manage.py`, and then run the following command `python manage.py runserver`.
Depending on your python set up, you may need to use `python3`.

If necessary, yuo can specify the port to run your server on by using `python manage.py runserver 0.0.0.0:PORT`
where `PORT` is the port you want the server to run on.

You can confirm your server is running by navigating to `http://127.0.0.1:8000/QuizManager/`
or `http://127.0.0.1:8000/admin/`. If you are running on a different port, change `8000` for the port of your choice.
The command output will also contain the URL, if needed.

You will be presented with a log in screen, and you will not be able to log in at this time.

#### Step 5, add users and groups

For ease of development and debugging, `quizmanager/StartUp.py` exists. Running the file will create the required groups
as well as a set of default users. It is **not** recommended that you use this file in production, as the passwords
are **not** secure. However, you can do so as the `create_users` function will only run if application is set to DEBUG
mode.

You may run `StartUp.create_groups` to create the required groups without worry. If you do use the file to generate
users, it is **highly recommended** that you change the passwords.

To create your first user, run the following command: `python manage.py createsuperuser`. Depending on your python set
up, you may need to use `python3`. You will be prompted for a username, email address, and password. These are required
to log in to the admin site. You can create normal users through the command line using `createuser` instead
of `createsuperuser`, however it is easier to do so from the GUI on the server.

Navigate to [http://127.0.0.1:8000/admin/](http://127.0.0.1:8000/admin/), or your equivalent, and log in. You will be
presented with two tables: one containing Users and Groups, and the other containing the models. It should look like
this:

![A screenshot of the admin page, displaying the User/Group table, and Answers/Questions/Quizzes table.](screenshots/admin_screenshot.png)

You can add Users and Groups using this GUI, as well as adding entities to the models. Clicking add for any item will
present you with a form of the required details. Further details can be added once they are created, such as first and
last names for users.

If you are using

#### Step 6, enjoy.

You have now installed and set up the application, and it is ready for use. You may wish to pre-populate it with sample
quizzes, questions and answers. To learn how to do this as a user, please see the User Guide included in this project.

If you are ready to deploy this to Production, or otherwise turn off debugging settings, create an environment variable
named `IS_PRODUCTION` and set its value to `True`. Django will detect the environment variable and turn off debugging
features. You may have to stop and then restart the server to full effect.

### Other Things

The trello kanban board can be found [here](https://trello.com/b/EK0IxF7u/project-management).